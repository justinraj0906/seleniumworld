package week2.day2;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Alert.html");
		driver.manage().window().maximize();
		
		driver.findElementByXPath("//button[text()='Prompt Box']").click();
		Alert alert=driver.switchTo().alert();
		String text=alert.getText();
		System.out.println(text);
		alert.accept();
		
		driver.findElementByXPath("//button[text()='Confirm Box']").click();
		Alert alert1=driver.switchTo().alert();
		String text1=alert.getText();
		System.out.println(text1);
		alert.accept();
		
		driver.findElementByXPath("//button[text()='Prompt Box']").click();
		Alert alert2=driver.switchTo().alert();
		String text2=alert.getText();
		System.out.println(text2);
		alert.accept();
		
				
		

	}

} 