package week2.day2;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alert=driver.switchTo().alert();
		
		String text=alert.getText();
		System.out.println(text);
		alert.sendKeys("justin");
		alert.accept();

		if(text.contains("justin")) {
			System.out.println("man");
		}
		
		
		
		
				
		

	}

} 