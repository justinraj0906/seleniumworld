package week2.day3;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;
public class IrctcSnap {

	public static void main(String[] args) {
		
{

			
				// TODO Auto-generated method stub
				System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
				ChromeDriver driver=new ChromeDriver();
				driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
				driver.manage().window().maximize();
				//link text have duplicates.but this is first option so no problem
				driver.findElementByLinkText("Contact Us").click();
				//by x path
				//driver.findElementByXPath("//a[text()='Contact Us']").click();
			Set<String> allWindows=driver.getWindowHandles();
			System.out.println(driver.getTitle());
			List<String> listOfWindows=new ArrayList<String>();
			listOfWindows.addAll(allWindows);
			driver.switchTo().window(listOfWindows.get(1));
			System.out.println(driver.getTitle());
			//driver.findElementByXPath(using)
			String address=driver.findElementByXPath("//b[text()='Registered Office / Corporate Office : ']//parent::p/following::p").getText();
			System.out.println(address);
			driver.switchTo().window(listOfWindows.get(0));
			driver.close();
			}

		}


	}

}
