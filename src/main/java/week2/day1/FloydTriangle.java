package week2.day1;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		//for users input 
		Scanner userinput=new Scanner(System.in);
		System.out.println("Enter your required no of rows for output : ");
		// store user variable using syntax
		int no=userinput.nextInt();
		//for purpose of increment values, created some int value
		int num=1;
		System.out.println("Your Wanted Floyd TRIANGLE  Shown Here : ");
		
		for(int i=1;i<=no;i++)
		{
			//this for loop is used for values incresing 1 2 3 like 
			for(int j=1;j<=i;j++)
			{
				System.out.print(num+" ");
				num++;
			}
			//this will continue by first forloop output with next row format
			System.out.println(" ");
			
		}


	}
}
