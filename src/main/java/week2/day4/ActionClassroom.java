package week2.day4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionClassroom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//Launch browser 
		ChromeDriver driver=new ChromeDriver();
		//load url
		driver.get("https://www.flipkart.com/"); 
		//maximize browser
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		WebElement main =driver.findElementByXPath("//span[text()='Electronics']");
		WebElement sub = driver.findElementByXPath("//a[text()='Asus']");
		//Actions move=new Actions(driver object);
		Actions move=new Actions(driver);
		move.moveToElement(main).perform();
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(sub)).click();
		
		

	}

}
