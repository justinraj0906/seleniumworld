package classroom1.day4;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnDropDown {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		// Launch browser
		ChromeDriver driver = new ChromeDriver();
		// load url
		driver.get("http://leaftaps.com/opentaps/");
		// maximize browser
		driver.manage().window().maximize();
		// enter username
		WebElement uName = driver.findElementById("username");
		uName.sendKeys("DemoSalesManager");
		// enter password
		driver.findElementById("password").sendKeys("crmsfa");
		// click login
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
		Select eleDropDown = new Select(eleSource);

		// eleDropDown.selectByVisibleText("Partner");
		// eleDropDown.selectByValue("LEAD_PARTNER");
		// eleDropDown.selectByIndex(6);
		List<WebElement> allOptions = eleDropDown.getOptions();
		int size = allOptions.size();
		eleDropDown.selectByIndex(size - 1);
		for (WebElement eachOption : allOptions) {
			System.out.println(eachOption.getText());

		}

		
		/*String cName = "TestLeaf";

		eleSource.sendKeys(cName);
		String vCname = driver.findElementById("viewLead_companyName_sp").getText();
		if (cName.equals(vCname)) {
			System.out.println("Matched");

		} else {
			System.out.println("Not Matched");
		}
*/
	}

}
