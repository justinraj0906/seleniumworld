package classroom1.day1;

public class LearnLoops {

	public static void main(String[] args) {

//		String name = "TestLeaf";
		String[] fishNames = { "Gold Fish", "Telescope", "Shark", "Angel" };

		for (int i = 0; i < fishNames.length; i++) {
			System.out.println(fishNames[i]);
		}

		for (String eachfish : fishNames) {
			System.out.println(eachfish);

		}
//		System.out.println(fishNames[1]);
//		System.out.println(fishNames[2]);

	}
}