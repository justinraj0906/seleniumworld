package classroom1.day2;

public class LearnObjects {
	
	public static void main(String[] args) {
//		ClassName objectName = new ClassName();
		
		
		sampleMethod();
		
		LearnMethods.getFishWeight();
		
		LearnMethods object = new LearnMethods();
		int ans = object.addTwoNumbers(11,12);
		System.out.println(ans);
		System.out.println(object.addTwoNumbers());
		String fishColor = object.getFishColor();
		
		if(fishColor.equals("Black")) 
			System.out.println("Shark");
		
		object.displayName();
		

	}

	public static void sampleMethod()
	{
		System.out.println("Hey I am from a sample method");
	}
	
	
}
