package classroom1.day3;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Login {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", 
				"./drivers/chromedriver.exe");
		// Launch browser
		ChromeDriver driver = new ChromeDriver();
		// load url
		driver.get("http://leaftaps.com/opentaps/");
		// maximize browser
		driver.manage().window().maximize();
		// enter username
		WebElement uName = driver.findElementById("username");
		uName.sendKeys("DemoSalesManager");
		// enter password
		driver.findElementById("password").sendKeys("crmsfa");
		// click login
		driver.findElementByClassName("decorativeSubmit").click();
		// click logout
		driver.findElementByClassName("decorativeSubmit").click();
		// close browser
		driver.close();

	}

}
