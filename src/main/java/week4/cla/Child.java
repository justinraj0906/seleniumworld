package week4.cla;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Child extends Parent {
	@BeforeTest
	public void setData() {
		excelFileName = "poc";
	}

	@Test(dataProvider = "fetchData")
	public void createLead(String cn, String fn, String ln) {
		
		driver.findElement(By.linkText("Create Lead")).click();
		driver.findElement(By.id("createLeadForm_companyName")).sendKeys(cn);
		driver.findElement(By.id("createLeadForm_firstName")).sendKeys(fn);
		driver.findElement(By.id("createLeadForm_lastName")).sendKeys(ln);
		driver.findElement(By.name("submitButton")).click();
	}

}
