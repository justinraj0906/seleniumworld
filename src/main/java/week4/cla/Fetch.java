package week4.cla;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Fetch {
	public static String[][] readExcel(String fileName) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook("./xml/" + fileName + ".xlsx");
		XSSFSheet sheet = workbook.getSheetAt(0);
		int rowcount = sheet.getLastRowNum();
		System.out.println("Row Count " + rowcount);
		int colCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column Count " + colCount);
		String[][] data = new String[rowcount][colCount];
		for (int i = 1; i <= rowcount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < colCount; j++) {
				data[i - 1][j] = row.getCell(j).getStringCellValue();
//				System.out.println(data);
			}
		}
		workbook.close();
		return data;
	}
}
