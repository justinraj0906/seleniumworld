package week4.day1;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateLead {
	ChromeDriver driver;





	// TODO Auto-generated method stub
	@Parameters({"usernam","pwd"})
	@BeforeMethod
	public void setupNew(String usernam,String pwd) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch browser

		//launch url

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps");

		driver.findElementById("username").sendKeys(usernam);

		driver.findElementById("password").sendKeys(pwd);

		
	}


	@AfterMethod
	public void tearDown() {
		driver.close();

	}


}

