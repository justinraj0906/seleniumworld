package week4.project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Project {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS );
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.findElementByXPath("//a[@title='Start your wonderful journey']").click();
		driver.findElementByXPath("//div[contains(text(),'Thuraipakkam')]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		Date date=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(date);
		int tomorrow=Integer.parseInt(today)+1;
		System.out.println(tomorrow);
		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		driver.findElementByXPath("//button[text()='Done']").click();
		
		
		List<WebElement> net = driver.findElementsByXPath("//div[@class='price']");
		System.out.println(net.size());
		List<String> price = new ArrayList<String>();
		for(WebElement each:net) {
			//get each price value
			String eachprice=each.getText(); 
			//add all price values into list "price"
			price.add(eachprice);
		
		}
		
		//total list of price printed
		System.out.println(price);
		//max price printed
		System.out.println(Collections.max(price));
		String maxprice=Collections.max(price);
		//regular expressions converted
		String better=maxprice.replaceAll("\\D","₹" );
		System.out.println(better); 
		//finded xpath for maxprice carname., use preceding to get that 
	WebElement carName = driver.findElementByXPath("//div[@class='price']/preceding::div[@class='details']/h3");

System.out.println("Expected car name : " + carName.getText());
	}

}
