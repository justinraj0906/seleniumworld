package week4.project;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LinkText {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS );
		driver.get("https://www.google.com/");
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.findElementByXPath("//input[@class='gLFyf gsfi']").sendKeys("raju");
		driver.findElementByXPath("//input[@value='Google Search']").click();
		List<WebElement> name = driver.findElementsByPartialLinkText("raju");
		for(WebElement eachname:name){
			System.out.println(eachname.getText());
		}


	}

}
