package classroom2.day4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		// Reference for ArrayList
		List<String> mentorsName = new ArrayList<>();
		// Add names
		mentorsName.add("Koushik");
		mentorsName.add("1");
		mentorsName.add("sarath");
		mentorsName.add("gopi");
		mentorsName.add("babu");
		mentorsName.add("balaji");
		mentorsName.add("preethy");
		mentorsName.add("koushik");
		mentorsName.add("preethy");
		System.out.println("Before sorting");
		System.out.println(mentorsName);
		System.out.println("After sorting");
		Collections.sort(mentorsName);
		System.out.println(mentorsName);

		System.out.println(mentorsName.get(mentorsName.size() - 1));

		System.out.println("List size: " + mentorsName.size());
		for (String eachName : mentorsName) {
			System.out.println(eachName);
		}
		mentorsName.remove("preethy");
		System.out.println(mentorsName);
		System.out.println(mentorsName.isEmpty());
		mentorsName.clear();
		System.out.println(mentorsName.size());

	}

}
