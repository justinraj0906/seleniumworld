package classroom2.day4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class LearnSet {

	public static void main(String[] args) {
		Set<String> mentorsName = new LinkedHashSet<>();
		// Add names
		mentorsName.add("Koushik");
		mentorsName.add("sarath");
		mentorsName.add("gopi");
		mentorsName.add("mohan");
		mentorsName.add("babu");
		mentorsName.add("balaji");
		mentorsName.add("preethy");
		mentorsName.add("koushik");
		mentorsName.add("preethy");
		System.out.println(mentorsName.size());
		int size = mentorsName.size();
		List<String> list = new ArrayList<>();
		list.addAll(mentorsName);
		System.out.println(list.get(size - 2));
		System.out.println(mentorsName);
		// not possible
		// System.out.println(mentorsName.get(mentorsName.size() - 1));

		System.out.println("List size: " + mentorsName.size());
		for (String eachName : mentorsName) {
			System.out.println(eachName);
		}
		mentorsName.remove("preethy");
	//	System.out.println(mentorsName);
		System.out.println(mentorsName.isEmpty());
		mentorsName.clear();
		System.out.println(mentorsName.size());

	}

}
