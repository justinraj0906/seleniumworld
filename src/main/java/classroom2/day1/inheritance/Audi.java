package classroom2.day1.inheritance;

public class Audi extends Car{
	
	@Override
	public void applyBrake() {
		super.applyBrake();
		System.out.println("ABS brake applied");		
	}
	
	public void musicPlayer() {
		System.out.println("playing music");
	}

}
