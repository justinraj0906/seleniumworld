package classroom2.day1.inheritance;

public class Car implements Vehicle{
	public void hasFourWheel() {
		System.out.println("4 wheels");
	}

	@Override
	public void applyBrake() {
		System.out.println("brake normal applied");		
	}

	@Override
	public void soundHorn() {
		System.out.println("Horned");
	}

}
