package classroom2.day1.inheritance;

public interface Vehicle {
	
	public void applyBrake();
	
	public  void soundHorn();

}
