package classroom2.day1.inheritance;

public class Auto implements Vehicle{
	public void hasThreeWheel() {
		System.out.println("3 wheels");
	}

	@Override
	public void applyBrake() {
		System.out.println("brake applied");		
	}

	@Override
	public void soundHorn() {
		System.out.println("Horned");
	}

}
