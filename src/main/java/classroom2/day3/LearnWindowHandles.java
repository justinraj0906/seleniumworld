package classroom2.day3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandles {

	public static ChromeDriver driver;
	static int i = 1;

	public static void takeSnap() throws IOException {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img" + i + ".png");
		FileUtils.copyFile(src, desc);
		i++;
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", 
				"./drivers/chromedriver.exe");
		// Launch browser
		driver = new ChromeDriver();

		driver.get("https://www.w3schools.com/js/js_popup.asp");
		takeSnap();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("(//a[text()='Try it Yourself �'])[3]").click();
		Set<String> allWindow = driver.getWindowHandles();
		// allWindow.size();
		System.out.println(driver.getTitle());
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindow);
		driver.switchTo().window(listOfWindow.get(1));
		// ScreenShot

		System.out.println(driver.getTitle());
		driver.switchTo().frame("iframeResult");

		driver.findElementByXPath("//button[text()='Try it']").click();
		takeSnap();

		/*
		 * int a= 0; for (String eachWindow : allWindow) { a++;
		 * driver.switchTo().window(eachWindow); if (a==2) { break;
		 * 
		 * }
		 * 
		 * }
		 */
		// Switch to 2nd window

	}
}
