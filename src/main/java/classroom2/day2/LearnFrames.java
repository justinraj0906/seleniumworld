package classroom2.day2;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrames {

	public static void main(String[] args) {
//		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://jqueryui.com/selectable/");
		driver.manage().window().maximize();
		// parent frame is used to move to the preceding frame
		// switching to the desired frame
		driver.switchTo().frame(0);
		driver.findElementByXPath("//li[text()='Item 3']").click();
		// takes you the main page
		driver.switchTo().defaultContent();
		driver.findElementByLinkText("Download").click();
		
		
		
		
		
		
		
	}

}
