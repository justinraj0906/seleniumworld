package week5.Table;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS );
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://erail.in/");
		WebElement from = driver.findElement(By.id("txtStationFrom"));
		from.clear();
		from.sendKeys("MAS",Keys.TAB);
		WebElement to = driver.findElement(By.id("txtStationTo"));
		to.clear();
		to.sendKeys("DLI",Keys.TAB);
		WebElement table = driver.findElement(By.xpath("//table[@class='DataTable TrainList']"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());	
		
       // System.out.println(datas.getText());
       // List<WebElement> datas = firstdata.findElements(By.tagName("td"));
        for(WebElement eachrow:rows) {
        	List<WebElement> col = eachrow.findElements(By.id("tr"));
        System.out.println(col.get(0).getText());	
        }
		}
        




	}

}
