package week5.day5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class MapProj {

	public static void main(String[] args) {
		String some="testleaf";
		char[] cha = some.toCharArray();
		Map<Character, Integer> map=new HashMap<Character, Integer>();	
		/*List<Integer> key = new ArrayList<Integer>();
		List<Character> achar = new ArrayList<Character>();*/
		for(Character each:cha) {
			//it gives that keys value ., if not assigned any value then go with else
			if(map.containsKey(each))
			{
				map.put(each, map.get(each)+1);
			}
			else
			{
				map.put(each, 1);
			}
			/*//to get integer
			key.add(map.get(each));
			//to get char
			achar.add(map.size()-1, each);*/
		}
		System.out.println(map);
		int i = 0;
		Character ch = null;
		Integer value = null;
		for (Entry<Character, Integer> entry : map.entrySet()) {
			value = entry.getValue();
			if(value > i) {
				i = value;
				 ch = entry.getKey();
						
			}
		}
		//this print max value with specific character
		System.out.println(i + "-> "+ ch);
//System.out.println(map.entrySet().);
		/*System.out.println(key);
		System.out.println(achar);
		System.out.println(map);*/

	}
}
