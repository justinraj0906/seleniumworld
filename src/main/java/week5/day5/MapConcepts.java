package week5.day5;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapConcepts {
	public static void main(String[] args) {
		Map<Character, String> name= new HashMap<Character,String>();
		name.put('a',"Aranga" );
		name.put('b',"Aravindh" );
		name.put('c',"Ajith" );
		name.put('d',"Arunachala" );
		name.put('e',"Ganga" );
		name.put('f',"Pradeep" );
		for(Entry<Character, String> eachname:name.entrySet()){
			//System.out.println(eachname);
			//System.out.println(eachname.getKey()+" "+eachname.getValue());
			String value = eachname.getValue();
			boolean starts = value.startsWith("A");
			if(starts)
			{
				System.out.println(value);
			}
			
		}
		
		
	}

}
