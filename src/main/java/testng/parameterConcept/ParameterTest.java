package testng.parameterConcept;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterTest {
@Test
@Parameters({"url","username","pass"})
	public void parameterTest(String url, String username,String pass) {
	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.get(url);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	driver.findElementByXPath("//*[@id='identifierId']").clear();
	driver.findElementByXPath("//*[@id='identifierId']").sendKeys(username);
	driver.findElementByXPath("//*[@class='RveJvd snByac']").click();
	driver.findElementByXPath("//*[@id='password']/div[1]/div/div[1]/input").sendKeys(pass);
	driver.findElementByXPath("(//div[@class='ZFr60d CeoRYc'])[1]").click();
	
		
	}
}

