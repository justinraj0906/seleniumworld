package week3.smallChallenges;

public class Rever {

	public static void main(String[] args) {
		String r1="Hello World";
		System.out.println("Reverse the Second Word In Given Sentence: "+ r1);
		System.out.println();
		String[] r =r1.split(" ");    //splited two words reference with ("space")
		StringBuffer sf=new StringBuffer(r[1]);  //now created object for StringBuffer  and pass the second word ("World") only 

		//now printed first word with conversion (using bufferobject.reverse) of second word
		//comp to String ., StringBuffer is "mutable" (so we use Reverse function in StringBuffer). But String is "immutable"

		System.out.println(r[0]+" " +sf.reverse());

	}

}
