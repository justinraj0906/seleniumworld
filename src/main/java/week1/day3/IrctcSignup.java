package week1.day3;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcSignup {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//Launch browser 
		ChromeDriver driver=new ChromeDriver();
		//load url
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf"); 
		//maximize browser
		driver.manage().window().maximize();
		
		//enter username
		driver.findElementById("userRegistrationForm:userName").sendKeys("justinrajsakthi@gmail.com");
		Thread.sleep(3000);
		//enter password
		driver.findElementById("userRegistrationForm:password").sendKeys("Rajubhai0906");
		//confirm password
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Rajubhai0906");
		//secuirty question
		driver.findElementById("userRegistrationForm:securityQ").sendKeys("What is your fathers middle name?");
		//security answer 
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("S");
		//preferred language
		driver.findElementById("userRegistrationForm:prelan").sendKeys("English");
		//firstname
		driver.findElementById("userRegistrationForm:firstName").sendKeys("justin");
		//middlename
		driver.findElementById("userRegistrationForm:middleName").sendKeys("raj");
		//lastname
		driver.findElementById("userRegistrationForm:lastName").sendKeys("K");
		//gender radio button option-0,1 represents which radio button, click option 
		driver.findElementById("userRegistrationForm:gender:0").click();
		//marital status
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		//using thread for timing for dob
		Thread.sleep(1500);
		//date
		driver.findElementById("userRegistrationForm:dobDay").sendKeys("03");
		//month
		driver.findElementById("userRegistrationForm:dobMonth").sendKeys("JUN");
		//year
		driver.findElementById("userRegistrationForm:dateOfBirth").sendKeys("1996");
		//occupation
		driver.findElementById("userRegistrationForm:occupation").sendKeys("Professional");
		//aadhar card no
		driver.findElementById("userRegistrationForm:uidno").sendKeys("");
		//PAN CARD NO
		driver.findElementById("userRegistrationForm:idno").sendKeys("");
		//select country
		driver.findElementById("userRegistrationForm:countries").sendKeys("India");
		//email
		driver.findElementById("userRegistrationForm:email").sendKeys("justinrajsakthi@gmail.com");
		//mobile no
		driver.findElementById("userRegistrationForm:mobile").sendKeys("7010972634");
		//nationality
		driver.findElementById("userRegistrationForm:nationalityId").sendKeys("India");
		//flat/door/block no
		driver.findElementById("userRegistrationForm:address").sendKeys("shree/11,12");
		//street/lane
		driver.findElementById("userRegistrationForm:street").sendKeys("natu veeratchi street/near koil");
		//area/locality
		driver.findElementById("userRegistrationForm:area").sendKeys("mylapore");
		//pincode
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600004",Keys.TAB);
		//city
		driver.findElementById("userRegistrationForm:cityName").isSelected();
		//postofficename
		driver.findElementById("userRegistrationForm:postofficeName").click();
		//landline
		driver.findElementById("userRegistrationForm:landline").sendKeys("8098597934");
		//copy res to off address
		driver.findElementById("userRegistrationForm:resAndOff:0").click();
		
	
		
		
		

	}

}
