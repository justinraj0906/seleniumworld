package week1.day7;

import java.util.Scanner;

public class Armstrong {

	public static void main(String[] args) {
		Scanner value=new Scanner(System.in);

		System.out.println("Enter the number you want to check : ");
		//user input
		int no=value.nextInt();
		int a,b,c,d,i;
		//To get 100 th digit
		a=no/100;
		b=no%100;
		//To get 10 th digit
		c=b/10;
		//To get units digit
		d=b%10;
		//Formula (100 th digit ^3+10 th digit ^3+ units digit ^3)
		i=(a*a*a+c*c*c+d*d*d);
		//if(given number==output number by formulae)
		if(no==i) {
			System.out.println("You entered number"  +" "+i + " is ARMSTRONG NUMBER");}
		else
			System.out.println("Sorry,You entered number is normal one");
		value.close();
	}

}
