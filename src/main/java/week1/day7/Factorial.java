package week1.day7;

import java.util.Scanner;

public class Factorial {

	private static Scanner input;

	public static void main(String[] args) {
		input = new Scanner(System.in);
		System.out.println("Enter the number");
		int no=input.nextInt();
		int fact=1;
		for(int i=1;i<=no;i++) 
		fact=fact*i;
		{
		System.out.println("Factorial of your entered number is:"+" "+fact );
		}
	}

}
