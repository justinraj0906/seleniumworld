package week1.day5;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select; 

public class Day3Selenium {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//launch browser
		ChromeDriver driver =new ChromeDriver();
		//launch url
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();

		WebElement uname = driver.findElementById("username");

		uname.sendKeys("DemoSalesManager");

		driver.findElementById("password").sendKeys("crmsfa");

		driver.findElementByXPath("//input[@type='submit']").click();

		driver.findElementByLinkText("CRM/SFA").click();

		driver.findElementByLinkText("Leads").click();

		driver.findElementByLinkText("Create Lead").click();

		//driver.findElementByXPath("//a[contains,text='Create Lead']").click();

		driver.findElementById("createLeadForm_companyName").sendKeys("Infosys");

		driver.findElementById("createLeadForm_firstName").sendKeys("Justin");


		driver.findElementById("createLeadForm_lastName").sendKeys("K");
		//Source dropdown
		WebElement source1 = driver.findElementById("createLeadForm_dataSourceId");
		Select drop1=new Select(source1);
		List<WebElement> click1 = drop1.getOptions();
		int size1=click1.size();
		drop1.selectByIndex(size1-1);
		/*String compname = driver.findElementById("viewLead_companyName_sp").getText();


		String firstname = driver.findElementById("viewLead_firstName_sp").getText();


		String lastname = driver.findElementById("viewLead_lastName_sp").getText();

		System.out.println(compname);

		System.out.println(firstname);
		System.out.println(lastname);*/

		//marketing dropdown	
		WebElement source2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select drop2=new Select(source2);
		List<WebElement> click2 = drop2.getOptions();
		int size2=click2.size();
		drop2.selectByIndex(size2-1);
		//ownership
		WebElement source3 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select drop3=new Select(source3);
		List<WebElement> click3 = drop3.getOptions();
		int size3=click3.size();
		drop3.selectByIndex(size3-1);
		//State
		WebElement source4 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select drop4=new Select(source4);
		List<WebElement> click4 = drop4.getOptions();
		int size4=click4.size();
		drop4.selectByIndex(size4-1);
		//Industry
		WebElement source5 = driver.findElementById("createLeadForm_industryEnumId");
		Select drop5=new Select(source5);
		List<WebElement> click5 = drop5.getOptions();
		int size5=click5.size();
		drop5.selectByIndex(size5-1);
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("justinraj");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("j.son");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("justinjj");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("655000");
		driver.findElementById("createLeadForm_sicCode").sendKeys("123");
		driver.findElementById("createLeadForm_description").sendKeys("best to in lead");
		driver.findElementById("createLeadForm_importantNote").sendKeys("creative thinking");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("600004");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("u9990809");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("justinrajsakthi@gmail.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("bbhb ");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("kmkmkmk");
		driver.findElementById("createLeadForm_generalCity").sendKeys("mylapore");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600004");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("60");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("raj");
		driver.findElementById("createLeadForm_departmentName").sendKeys("SELENIUM");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("40");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("$$");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("7010972634");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("justin");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://leaflaps.com");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("rajubhai");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("ghjghg");

		driver.findElementByName("submitButton").click();







	}




















}

