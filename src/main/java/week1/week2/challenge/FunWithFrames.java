package week1.week2.challenge;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class FunWithFrames {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/frame.html");
		driver.manage().window().maximize();
		
		//first frame
		driver.switchTo().frame(0);
		//use findElementById bcoz this is "UNIQUE" even after clicking the button.
		WebElement touch = driver.findElementById("Click");
		System.out.println("Text before the click action :");
		System.out.println("  " +touch.getText());
		touch.click();
		System.out.println("Text after the click action :");
		System.out.println("  " +touch.getText());
		driver.switchTo().defaultContent();

		//2 nd frame
		driver.switchTo().frame(1);
		//nested frame inside the 2 nd frame
		driver.switchTo().frame("frame2");
		//use findElementById bcoz this is unique even after clicking the button.
		WebElement enter = driver.findElementById("Click1");
		System.out.println("  ");
		System.out.println("Text before the click action :");
		System.out.println("  " +enter.getText());
		enter.click();
		System.out.println("Text after the click action :");
		System.out.println("  " +enter.getText());
		driver.switchTo().defaultContent();

		//3rd frame		
		driver.switchTo().frame(2);
		//nested frame inside the 2 nd frame
		driver.switchTo().frame("frame2");
		WebElement msg = driver.findElementByTagName("body");
		System.out.println("  ");
		System.out.println("  " +msg.getText());
		System.out.println("3 MAIN Frames 2 NESTED Frames");



		// button = driver.findElementById("Click Me");
		//button.click();











	}

}
