package week1.week2.challenge;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebelementHighlight {

	public static void main(String[] args) throws Exception{
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://jqueryui.com/selectable/"); 
		driver.manage().window().maximize();
		driver.switchTo().frame(0);
		driver.findElementByXPath("//li[text()='Item 1']").click();
		driver.getKeyboard().sendKeys(Keys.CONTROL);
		driver.findElementByXPath("//li[text()='Item 3']").click();
		File hit = driver.getScreenshotAs(OutputType.FILE);
		File hitback = new File("./snaps/img.png");//("./ file location inside project/image name.format");
		FileUtils.copyFile(hit,hitback);
	}

}
