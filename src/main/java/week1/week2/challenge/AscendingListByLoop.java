package week1.week2.challenge;

import java.util.ArrayList;
import java.util.List;

public class AscendingListByLoop {

	public static void main(String[] args) {
		List<String> mBrands=new ArrayList<String>();
		mBrands.add("Vivo");
		mBrands.add("Oppo");
		mBrands.add("Apple");
		mBrands.add("Redmi");
		mBrands.add("Realme");
		mBrands.add("Asus");
		mBrands.add("Motorola");
		String temp=null;
		System.out.println(" ");
		System.out.println(mBrands);
		System.out.println(" ");
		for(String eachBrand:mBrands)
		{
			System.out.println(eachBrand);
		}
		System.out.println(" ");
		System.out.println("         Ascending Order :  ");
		for(int i=0;i<mBrands.size();i++)
		{
			for(int j=i+1;j<mBrands.size();j++)
			{
				if(mBrands.get(i).compareTo(mBrands.get(j))>0)
				{
					temp=mBrands.get(i);
					mBrands.set(i,mBrands.get(j));
					mBrands.set(j, temp);
				}

			}
		}
		System.out.println(mBrands);
		System.out.println(" ");
		for(String eachBrand:mBrands)
		{
			System.out.println(eachBrand);
		}


	}

}
