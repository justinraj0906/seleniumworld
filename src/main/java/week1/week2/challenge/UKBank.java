package week1.week2.challenge;

public interface UKBank {
	int min_bal=500;
	public void debit();
	public void credit();
	public void transferMoney();

}
