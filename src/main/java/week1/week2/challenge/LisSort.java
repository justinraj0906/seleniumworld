package week1.week2.challenge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LisSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
List<String> mBrands=new ArrayList<String>();
mBrands.add("Vivo");
mBrands.add("Oppo");
mBrands.add("Apple");
mBrands.add("Redmi");
mBrands.add("Realme");
mBrands.add("Asus");
mBrands.add("Motorola");
System.out.println(mBrands);
System.out.println(" ");
System.out.println("         Ascending Order :  ");
Collections.sort(mBrands);
System.out.println(mBrands);
System.out.println(" ");
for(String eachBrand:mBrands)
{
	System.out.println(eachBrand);
}
mBrands.remove(0);
mBrands.remove(0);
System.out.println("");
System.out.println("    After removing some Brands- Descending Order :  ");
Collections.reverse(mBrands);
System.out.println(mBrands);
System.out.println("");
for(String eachBrand:mBrands)
{
	System.out.println(eachBrand);
}
}
}