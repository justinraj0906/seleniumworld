package week1.week2.challenge;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class IndeedFresher {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//Launch browser 
		ChromeDriver driver=new ChromeDriver();
		//load url
		driver.get("https://www.indeed.co.in/Fresher-jobs"); 
		//maximize browser
		driver.manage().window().maximize();
		System.out.println(driver.getTitle());

		for(int i=1;i<17;i++) {
			WebElement sub=driver.findElementByXPath("(//div[@class='title']//a)["+i+"]");
			Actions action=new Actions(driver);
			action.sendKeys(Keys.CONTROL).click(sub).perform();
			Set<String> win=driver.getWindowHandles();
			Iterator<String> iter=win.iterator();
			String s=iter.next();
			System.out.println("Parent website Id" + s);
			String s1=iter.next();
			System.out.println("Child website Id" + s1);
			driver.switchTo().window(s1);
			System.out.println("Child window Title  : "+ driver.getTitle());
			System.out.println();
			driver.close();
			driver.switchTo().window(s);
			//			System.out.println("Parent window Title :" +driver.getTitle());
		}

		System.out.println("Parent Window Title: " +driver.getTitle());
		driver.quit();




	}

}
//span[@id='indeed-ia-1555076067386-0label']
//a[@class='indeed-apply-button']