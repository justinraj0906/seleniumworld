package week1.day2;

public class Calans {

	public static void main(String[] args) {
		// Addition using method 
		//classname object=new classname();
		Calculator sum=new Calculator();
		//int var=object.methodname(value);
		int ansOfSum=sum.addTwoNumbers(20, 5);
		System.out.println(ansOfSum);
		//Subtraction using method
		//classname object=new classname();
		Calculator sub=new Calculator();
		//int var=object.methodname(value);
		int ansOfSub=sub.subTwoNumbers(20, 5);
		System.out.println(ansOfSub);
		//Mutiplication using method
		//classname object=new classname();
		Calculator mul=new Calculator();
		//int var=object.methodname(value);
		int ansOfMul=mul.mulTwoNumbers(20, 5);
		System.out.println(ansOfMul);
		//Division using method
		//classname object=new classname();
		Calculator div=new Calculator();
		//int var=object.methodname(value);
		int ansOfDiv=div.divTwoNumbers(20, 5);
		System.out.println(ansOfDiv);

	}

}
