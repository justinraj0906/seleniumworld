Feature: LeafTaps login flow
Scenario: Positive flow
Given Launch the Browser
And Load the URL
And Maximize the Browser
And Set Timeouts
And Enter Username
And Enter Password
When Click Login
Then Verify Browser
