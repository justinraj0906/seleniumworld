package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;
@CucumberOptions(features="src/test/java/feature/Feature.feature",glue="",dryRun=false, snippets=SnippetType.CAMELCASE, monochrome=true)

public class TestRunner extends AbstractTestNGCucumberTests {
	
}


