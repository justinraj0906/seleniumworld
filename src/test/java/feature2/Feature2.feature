Feature: LeafTaps login flow
Background:
 Given Launch the Browser
And Load the URL
And Maximize the Browser
And Set Timeouts

Scenario Outline: Positive flow
And Enter Username as "<username>"
And Enter Password as "<password>"
When Click Login 		
Then Verify login successful

Examples:
|username|password|
|DemoSalesManager|crmsfa|

Scenario Outline: Neagtive flow
And Enter Username as "<username>"
And Enter Password as "<password>"
When Click Login
But Verify login unsuccessful

Examples:
|username|password|
|DemoSalesManager|crmsfa|