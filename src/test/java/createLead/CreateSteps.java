package createLead;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateSteps {
	public ChromeDriver driver;
	@Given("Launch the Browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		
	}

	@And("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps/");
	}

	@And("Maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@And("Set Timeouts")
	public void setTimeouts() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@And("Enter Username")
	public void enterUsername() {
WebElement uname = driver.findElementById("username");
		
		uname.sendKeys("DemoCSR");
	}

	@And("Enter Password")
	public void enterPassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@And("Click Login")
	public void clickLOgin() {
		driver.findElementByXPath("//input[@type='submit']").click();
	}
	@Given("Values Sends to target")
	public void valuesSendsToTarget() throws InterruptedException {
driver.findElementByLinkText("CRM/SFA").click();
		
		Thread.sleep(4000);
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Create Lead").click();
		
		//driver.findElementByXPath("//a[contains,text='Create Lead']").click();
		
		Thread.sleep(2000);
		
		String cname = "BBC";

		
		WebElement Compname = driver.findElementById("createLeadForm_companyName");
				
				Compname.sendKeys(cname);

		driver.findElementById("createLeadForm_firstName").sendKeys("Jordi");
		
		driver.findElementById("createLeadForm_lastName").sendKeys("Alba");
		
		driver.findElementById("createLeadForm_dataSourceId").sendKeys("Cold Call");
		
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Jordi");
		
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Alba");
		
		driver.findElementById("createLeadForm_personalTitle").sendKeys("test");
		
		
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("JOURNALIST");
		
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");
		
		driver.findElementById("createLeadForm_industryEnumId").sendKeys("Press");
		
		
		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("50");
		
		Select eleownership = new Select (driver.findElementById("createLeadForm_ownershipEnumId"));
		
		eleownership.selectByIndex(2);
		
		
		driver.findElementById("createLeadForm_sicCode").sendKeys("13345");
		
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("&*@$");
		
		
		driver.findElementById("createLeadForm_description").sendKeys("description");
		
		driver.findElementById("createLeadForm_importantNote").sendKeys("text note");
		
		//contact information
		
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("001");
		
		
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("91");
		
		WebElement phonenum = driver.findElementById("createLeadForm_primaryPhoneNumber");

		String mobile = "9978455211";
		phonenum.sendKeys(mobile);
		
		
		
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("abc@testleaf.com");
		
		
		
		
	//Primary Address
		
		driver.findElementById("createLeadForm_generalToName").sendKeys("Jordi");
		
		
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("100,1st Lane");
		
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("5th cross st");
		
		
		driver.findElementById("createLeadForm_generalCity").sendKeys("Miami");
		
		
		
		driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("Florida");
		
		
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("33101");
	}

	@When("Submit")
	public void submit() {
		driver.findElementByXPath("//input[@name='submitButton']").click();
	}
	@Then("Validation")
	public void validation() throws InterruptedException  {
		String cname = "BBC";
		Thread.sleep(2000);
		String vcompname =driver.findElementById("viewLead_companyName_sp").getText();
		
		System.out.println(vcompname);
		
		if (vcompname.contains(cname)) {
			
			System.out.println("matched");
			
		}	else {
				
				System.out.println("NotMatched");
				
				
			}
			
		driver.quit();
		
	}
	

		
		
		
	}


