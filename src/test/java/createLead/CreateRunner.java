package createLead;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;
@CucumberOptions(features="src\\test\\java\\createLead\\CreateFeature.feature", glue="createLead",dryRun=false ,snippets=SnippetType.CAMELCASE,monochrome=true)
public class CreateRunner extends AbstractTestNGCucumberTests  {

}
