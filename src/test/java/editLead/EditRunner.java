package editLead;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;
@CucumberOptions(features="src\\test\\java\\editLead\\EditFeature.feature", glue="editLead",dryRun=false ,snippets=SnippetType.CAMELCASE,monochrome=true)
public class EditRunner extends AbstractTestNGCucumberTests  {

}
