package runner2;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;
@CucumberOptions(features="src/test/java/feature/Feature.feature",glue="steps2",dryRun=false, snippets=SnippetType.CAMELCASE, monochrome=true)

public class TestRunner2 extends AbstractTestNGCucumberTests {
	
}


