package steps2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps2 {
	public ChromeDriver driver;
	@Given("Launch the Browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		
	}

	@And("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps/");
	}

	@And("Maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@And("Set Timeouts")
	public void setTimeouts() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@And("Enter Username")
	public void enterUsername() {
WebElement uname = driver.findElementById("username");
		
		uname.sendKeys("DemoCSR");
	}

	@And("Enter Password")
	public void enterPassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@When("Click LOgin")
	public void clickLOgin() {
		driver.findElementByXPath("//input[@type='submit']").click();
	}

	

	@Then("Verify Browser")
	public void verifyBrowser() {
	    System.out.println("Login is successfull");
	}
	
	@But("Verify Browser")
	public void verifyBrowser() {
	    System.out.println("Login is unsuccessfull");
	}
	
	

}
